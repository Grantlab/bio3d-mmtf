#' Download MMTF Coordinate Files
#'
#' @param ids  A character vector of one or more 4-letter PDB
#'    codes/identifiers or 6-letter PDB-ID_Chain-ID of the files to be
#'    downloaded, or a \sQuote{blast} object containing \sQuote{pdb.id}.
#' @param path The destination path/directory where files are to be written. 
#' @param URLonly logical, if TRUE a character vector containing the URL
#'    path to the online file is returned and files are not downloaded. If
#'    FALSE the files are downloaded.
#' @param overwrite logical, if FALSE the file will not be downloaded if
#'    it alread exist.
#' @param verbose print details of the reading process. 
#' @return Returns a list of successfully downloaded files. Or optionally if URLonly
#'  is TRUE a list of URLs for said files.
#' @details This is a basic function to automate file download from the PDB.
#' 
#' @references  Grant, B.J. et al. (2006) \emph{Bioinformatics} \bold{22}, 2695--2696.
#'
#' Documentation of MMTF: \url{https://mmtf.rcsb.org/}.
#' @seealso
#'    \code{\link{get.mmtf}}, \code{\link{get.pdb}},
#'    \code{\link{read.pdb}}, \code{\link{write.pdb}},
#'    \code{\link{atom.select}}, \code{\link{trim.pdb}}
#' @examples
#' \dontrun{ 
#'   ## Download PDB file
#'   p <- get.mmtf("5p21")
#'   read.mmtf(p)
#' }



"get.mmtf" <- function (ids, path = ".", URLonly = FALSE, overwrite = FALSE, 
                        verbose = TRUE) {

    if(inherits(ids, "blast")) ids = ids$pdb.id

    if (any(nchar(ids) < 4))
      stop("ids should be standard 4 character PDB-IDs or 6 character PDB-ID_Chain-IDs")

    ids4 = ids
    if (any(nchar(ids) > 4)) {
      ids4 <- unlist(lapply(strsplit(ids, "_"), function(x) x[1]))
      
      if (any(nchar(ids4) > 4))
        warning("ids should be standard 4 character PDB-IDs: trying first 4 characters...")

      ids4 <- substr(basename(ids), 1, 4)
    }
    ids4 <- toupper(unique(ids4))
    pdb.files <- paste(ids4, ".mmtf.gz", sep = "")
    get.files <- file.path("https://mmtf.rcsb.org/v1.0/full", pdb.files)
    if (URLonly) 
        return(get.files)
    put.files <- file.path(path, pdb.files)

    if(!file.exists(path))
       dir.create(path)
    rtn <- rep(NA, length(pdb.files))

    for (k in 1:length(pdb.files)) {
        # check if either gzipped or unzipped version exists
        check_files <- c(
            sub(".gz$", "", put.files[k]),
            put.files[k]
        )
        
        # download if needed, else use existing file
        if (!any(file.exists(check_files)) | overwrite ) {
            rt <- try(download.file(get.files[k], put.files[k], quiet = !verbose), silent=TRUE)
            rtn[k] <- rt
            if(inherits(rt, "try-error")) {
                rtn[k] <- 1
                file.remove(put.files[k])
            }
        }
        else {
            put.files[k] <- check_files[ file.exists(check_files) ][1]
            rtn[k] <- put.files[k]
            warning(paste(put.files[k], "exists. Skipping download"))
        }

        # gunzip file if needed
        #if(summary(file(put.files[k]))$class == "gzfile" | overwrite) {
        fe <- file.exists(check_files)
        if(fe[2] &! fe[1] | overwrite) {
            cmd <- paste("gunzip -f", put.files[k])
            system(cmd)
            rtn[k] <- sub(".gz$", "", put.files[k])
        }
        gc()
    }
   
    rtn <- as.character(rtn) 
    names(rtn) <- file.path(path, paste(ids4, ".", "mmtf", sep = ""))
    if (any(rtn == '1')) {
        warning("Some files could not be downloaded, check returned value")
        return(rtn)
    } else {
        return(names(rtn))
    }
}
