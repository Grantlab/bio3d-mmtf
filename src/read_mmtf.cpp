#include <mmtf.hpp>
#include <Rcpp.h>
#include <string>
#include <math.h>
using namespace std;
using namespace Rcpp;



// [[Rcpp::export('.read_mmtf')]]
List read_mmtf(std::string filename) {

  // out is a List object
  Rcpp::List out;
  
  mmtf::StructureData sd;
  mmtf::decodeFromFile(sd, filename);

  // generic info
  string mmtfVersion = sd.mmtfVersion;
  string mmtfProducer = sd.mmtfProducer;

  // printopt("unitCell", "%g", example.unitCell, true);
  //string spaceGroup = sd.spaceGroup;
  //string structureId = sd.structureId;
  string title = sd.title;
  // printopt("depositionDate", example.depositionDate, true);
  // printopt("releaseDate", example.releaseDate, true);


  
  //std::ostringstream out;
  int modelIndex = 0;
  int chainIndex = 0;
  int groupIndex = 0;
  int atomIndex = 0;

   // assign vectors for building final 'atom' object
  vector<string> type;
  vector<int> eleno;
  vector<string> elety;
  vector<char> alt;
  vector<string> resid;
  vector<string> chain;
  vector<int> resno;
  vector<char> insert;
  vector<double> x;
  vector<double> y;
  vector<double> z;
  vector<double> o;
  vector<double> b;
  vector<string> segid;
  vector<string> elesy;
  vector<double> charge;

  // xyz object
  vector<double> xyz;
 
  // traverse models
  for (int i = 0; i < sd.numModels; i++, modelIndex++) {
    // traverse chains
    for (int j = 0; j < sd.chainsPerModel[modelIndex]; j++, chainIndex++) {
      // traverse groups
      for (int k = 0; k < sd.groupsPerChain[chainIndex]; k++, groupIndex++) {

	const mmtf::GroupType& group =
	  sd.groupList[sd.groupTypeList[groupIndex]];
	int groupAtomCount = group.atomNameList.size();

        for (int l = 0; l < groupAtomCount; l++, atomIndex++) {

	  // assume atom records are the same for all models
	  // store only xyz
	  if (i == 0) {

	    // ATOM or HETATM
	    if (mmtf::is_hetatm(group.chemCompType.c_str())) {
	      type.push_back("HETATM");
	    }
	    else {
	      type.push_back("ATOM  ");
	    }

	    // Atom serial
	    eleno.push_back(sd.atomIdList[atomIndex]);

	    // Atom name
	    elety.push_back(group.atomNameList[l]);

	    // Alternate location
	    alt.push_back(sd.altLocList[atomIndex]);

	    // Insert code
	    insert.push_back(sd.insCodeList[groupIndex]);
	    
	    // Group name
	    resid.push_back(group.groupName);
	  	  
	    // Chain name
	    chain.push_back(sd.chainIdList[chainIndex]);

	    // Group serial 
	    resno.push_back(sd.groupIdList[groupIndex]);

	    // Occupancy
	    o.push_back(sd.occupancyList[atomIndex]);

	    // Beta factor
	    b.push_back(sd.bFactorList[atomIndex]);
	  
	    // Element
	    elesy.push_back(group.elementList[l]);

	    // Charge
	    charge.push_back(group.formalChargeList[l]);
	    
	    // x, y, z
	    x.push_back(sd.xCoordList[atomIndex]);
	    y.push_back(sd.yCoordList[atomIndex]);
	    z.push_back(sd.zCoordList[atomIndex]);
	  }
	    
	  xyz.push_back(sd.xCoordList[atomIndex]);
	  xyz.push_back(sd.yCoordList[atomIndex]);
	  xyz.push_back(sd.zCoordList[atomIndex]);
	}
      }
    }
  }

  out = Rcpp::List::create(Rcpp::Named("atom")=
			   Rcpp::DataFrame::create(
						   Rcpp::Named("type")=type,
						   Rcpp::Named("eleno")=eleno,
						   Rcpp::Named("elety")=elety,
						   Rcpp::Named("alt")=alt,
						   Rcpp::Named("resid")=resid,
						   Rcpp::Named("chain")=chain,
						   Rcpp::Named("resno")=resno,
						   Rcpp::Named("insert")=insert,
						   Rcpp::Named("x")=x,
						   Rcpp::Named("y")=y,
						   Rcpp::Named("z")=z,
						   Rcpp::Named("o")=o,
						   Rcpp::Named("b")=b,
						   Rcpp::Named("elesy")=elesy,
						   Rcpp::Named("charge")=charge,
						   Rcpp::Named("stringsAsFactors")=false
						   ),
			   Rcpp::Named("xyz")=xyz,
			   Rcpp::Named("models")=sd.numModels,
			   Rcpp::Named("title")=title
			   );
  return out;
}
