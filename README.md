# Macromolecular Transmission Format (MMTF) Reader module for Bio3D #

## Installation

```
#!r
install.packages("devtools")
devtools::install_bitbucket("Grantlab/bio3d-mmtf")
```

## Usage

```
#!r
pdb <- read_mmtf("1hel")
```
